<?php

require_once "Mobel.php";
$e = new Model;
$ex = $e->get_categories();

require "begin.html";
?>
<h1>Ajouter un noble</h1>
<form action="add.php" method="post">
    <label>Name</label><input type="text" name="name"><br><br>
    <label>Year</label><input type="number" name="year"><br><br>
    <label>Birthdate</label><input type="date" name="bd"><br><br>
    <label>Birthplace</label><input type="text" name="bp"><br><br>
    <label>Country</label><input type="text" name="country"><br><br>
    <?php
    function sp($s)
    {
        return htmlspecialchars($s, ENT_QUOTES);
    };
    foreach ($ex as $e) {
        echo '<label> <input type="radio" name="category" value="' . sp($e) . '"/>' . sp($e) . "</label>";
    }
    ?><br><br>
    <textarea name="motivation" cols="50" rows="5"></textarea><br><br>
    <input type="submit" value="Add in database">
</form>