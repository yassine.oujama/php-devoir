<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    require_once "Mobel.php";
    $model = new Model();
    if (isset($_GET["id"])) {
        $id = $_GET["id"];
        $nobel = $model->get_nobel_prize_informations($id);
        if ($nobel) {
            echo "<form action='updateNobl.php' method='post'>";
            echo "<label for='id'>ID </label>";
            echo "<input name='id' value='{$nobel['id']}' /><br />";
            echo "<label for='name'>Name</label>";
            echo "<input type='text' name='name' id='name' value='{$nobel['name']}' /><br />";
            echo "<label for='birthdate'>Birthdate</label>";
            echo "<input type='text' name='birthdate' id='birthdate' value='{$nobel['birthdate']}' /><br />";
            echo "<label for='birthplace'>Birthplace</label>";
            echo "<input type='text' name='birthplace' id='birthplace' value='{$nobel['birthplace']}' /><br />";
            echo "<label for='country'>Country</label>";
            echo "<input type='text' name='country' id='country' value='{$nobel['county']}' /><br />";
            echo "<label for='category'>Category</label>";
            echo "<input type='text' name='category' id='category' value='{$nobel['category']}' /><br />";
            echo "<label for='year'>Year</label>";
            echo "<input type='text' name='year' id='year' value='{$nobel['year']}' /><br />";
            echo "<label for='motivation'>Motivation</label>";
            echo "<textarea name='motivation' id='motivation'>{$nobel['motivation']}</textarea><br />";
            echo "<input type='submit' name='submit' value='Update' />";
            echo "</form>";

        } else {
            echo "There is no nobel prize with such id.";
        }
    }
?>
</body>
</html>