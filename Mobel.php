<?php
class Model
{
    public $connexion;

    public function __construct()
    {
        try {
            if ($this->connexion = new PDO(
                'mysql:host=localhost;dbname=G_nobel;charset=utf8',
                'root',
                ''
            )) {
            } else {
                throw new Exception('Unable to connect');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function get_last()
    {
        $req = $this->connexion->query('SELECT * FROM nobels ORDER BY year DESC LIMIT 25');
        return $req;
    }
    public function get_nb_nobel_prizes()
    {
        $req = $this->connexion->query('SELECT COUNT(*) FROM nobels');
        $z = $req->fetch();
        return $z[0];
    }
    public function get_nobel_prize_informations($id)
    {
        $requete = $this->connexion->query("Select * from nobels WHERE id =$id");
        return $requete->fetch();
    }
    public function get_categories()
    {
        $requete = $this->connexion->query('SELECT * FROM categories');
        $categorie = [];
        foreach ($requete as $re) {
            $categorie[] = $re[0];
        }
        return $categorie;
    }
    public function add_nobel_prize()
    {
        $year =  $_REQUEST['year'];
        $categorie = $_POST['category'];
        $name = $_POST['name'];
        $bd = $_POST['bd'];
        $bp = $_POST['bp'];
        $country = $_POST['country'];
        $motivation = $_POST['motivation'];
        $requete = $this->connexion->query("INSERT INTO nobels(year, category, name, birthdate, birthplace, county, motivation) 
        VALUES('$year','$categorie','$name','$bd', '$bp', '$country','$motivation')");

        return $requete;
    }
    public function remove_nobel_prize($id)
    {
        $requete = $this->connexion->query("delete from nobels WHERE id =$id");
        return $requete->fetch();
    }
    public function updateNobelPrize($info) {
        $sql = "UPDATE nobels SET year = :year, category = :category, name = :name, birthdate = :birthdate, birthplace = :birthplace, county = :county, motivation = :motivation WHERE id = :id";
        $stmt = $this->connexion->prepare($sql);
        $stmt->execute(array(
            ':year' => $info['year'],
            ':category' => $info['category'],
            ':name' => $info['name'],
            ':birthdate' => $info['birthdate'],
            ':birthplace' => $info['birthplace'],
            ':county' => $info['county'],
            ':motivation' => $info['motivation'],
            ':id' => $info['id']
        ));

        // $year = $info['year'];
        // $category = $info['category'] ;
        // $name = $info['name'];
        // $birthdate = $info['birthdate'];
        // $birthplace = $info['birthplace'];
        // $county = $info['county'];
        // $motivation = $info['motivation'];
        // $id = $info['id'];

        // $sql = "UPDATE nobels SET year = $year, category = $category, name = $name, birthdate = $birthdate, birthplace = $birthplace, county = $county, motivation = $motivation WHERE id = $id";
        // $res = $this->connexion->exec($sql);
        // return $res;

    }

}
