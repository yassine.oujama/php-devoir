<?php  
require_once "Mobel.php";
$model = new Model();

if (isset($_POST['submit'])) {
    function check_data() {
        $regex = '/^\s*$/';
        if (
            !isset($_POST['id']) || $_POST['id'] < 0 ||
            !isset($_POST['name']) || preg_match($regex, $_POST['name']) ||
            !isset($_POST["category"]) || preg_match($regex, $_POST['category']) ||
            !isset($_POST["year"]) || $_POST['year'] < 0
        ) {
            return false;
        } else {
            return true;
        }
    }

    if (check_data()) {
        $info = array(
            "id" => $_POST['id'],
            "name" => $_POST['name'],
            "birthdate" => $_POST['birthdate'],
            "birthplace" => $_POST['birthplace'],
            "year" => $_POST['year'],
            "county" => $_POST['country'],
            "category" => $_POST['category'],
            "motivation" => $_POST['motivation']
        );
        $model->updateNobelPrize($info);
        echo "The Nobel prize was updated successfully.";
        header("Location:last.php");


    } else {
        echo "The id, name, category and year are required. The year must be a positive number.";
    }
} else {
    echo "There is no nobel prize to update.";
}
?>






























































    
    <!-- if (isset($_POST['submit'])) {
        if(isset($_POST["id"])){
        function check_data(){
            if($_POST['id']>=0 && !empty($_POST['name'])   && $_POST['year'] > 0){
                $infos = array (
                    'id' => $_POST['id'],
                    'year' => $_POST['year'],
                    'category' => $_POST['category'],
                    'name' => $_POST['name'],
                    'birthdate' => $_POST['birthdate'],
                    'birthplace' => $_POST['birthplace'],
                    'contry' => $_POST['county'],
                    'text' => $_POST['text'],
                );
                require_once "mobel.php";
                $Obj = new Model();
                $res = $Obj->update_Nobel_Prize($infos);
                if ( $res ){
                    header("Location:last25.php?message=base_de_données_a_ete_mise_a_jour");
                }
                else{
                    echo "err";
                }
            }else{
                echo "no check";
            }
        };
        check_data();
    } else {
        echo "no url";
    }
    } -->
   